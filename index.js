// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
var SimpleSendGridAdapter = require('parse-server-sendgrid-adapter');
var simpleSendGridAdapter = new SimpleSendGridAdapter({
    apiKey: process.env.SENDGRID_KEY || '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',
    domain: process.env.DOMAIN || 'app.place-me.co.uk',
    fromAddress: process.env.MAILGUN_FROM_ADDRESS || 'alfo@crystal-bits.co.uk'
});

//var databaseUri = process.env.DATABASE_URI || process.env.MONGOLAB_URI;
var databaseUri = 'mongodb://lisophorm:k0st0golov@ds015329-a0.mlab.com:15329,ds015329-a1.mlab.com:15329/heroku_ns4wxp1j?replicaSet=rs-ds015329';

if (!databaseUri) {
    console.log('MONGOLAB_URI not specified, falling back to localhost.');
}

var api = new ParseServer({
    databaseURI: databaseUri || 'mongodb://localhost:27017/local',
    cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
    appId: process.env.APP_ID || 'y1V7z1DU1hAmSDWc7PrIGaExQvcRYmPU8DJbvtg5',
    masterKey: process.env.MASTER_KEY || 'MrfTOhA4zXI0z5J80odxcK7blOXYmRad845qq438', //Add your master key here. Keep it secret!
    serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
    liveQuery: {
        classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
    },
    basePath: process.env.BASE_PATH || 'http://localhost:1337/',
    verifyUserEmails: false,
    publicServerURL: process.env.PUBLIC_SERVER_URL || 'http://localhost:1337/parse',

    appName: process.env.APP_NAME || 'Place-ME',

    emailAdapter: simpleSendGridAdapter,
    customPages: {
        //invalidLink: 'http://yourdomain.com/invalid_link.html',
        //verifyEmailSuccess: 'http://yourdomain.com/verify_email_success.html',
        choosePassword: process.env.CHOOSE_PASSWORD_URL || 'http://localhost:3000/pages/auth/reset-password',
        passwordResetSuccess: process.env.PASSWORD_RESET_SUCCESS_URL|| 'http://localhost:3000/pages/auth/login?changepass=true'
    }


});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

console.log(api.basePath);

var app = express();

// Serve static assets from the /public folder
app.use('/', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// views is directory for all template files
app.set('views', __dirname + '/cloud/views');
app.set('view engine', 'ejs');

// https request
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
app.use(parseExpressHttpsRedirect());

//moment for datetime shit
var moment = require('moment');
var shortDateFormat = "DD/MM/YYYY"; // this is just an example of storing a date format once so you can change it in one place and have it propagate
app.locals.moment = moment; // this makes moment available as a variable in every EJS page
app.locals.shortDateFormat = shortDateFormat;

var querystring = require('querystring');

// core o functionalities

var sendgrid = require("./cloud/sendgrid-parse");
sendgrid.initialize("hybridrecruitment", "k0st0golov");

var caccamo='ciaO';
module.exports.caccamo=caccamo;

app.get('/testo', function (req, res) {
    Parse.Cloud.useMasterKey();
    console.log("searchin for ");
    console.log(req.query.userid);
    var response = "";
    var queryCode = new Parse.Query('UserInfo');
    queryCode.equalTo('objectId', req.query.userid);
    queryCode.first().then(function (obj) {
        if (typeof obj !== 'undefined') {
            console.log("USER ID:");
            console.log(obj);
            res.render('dummy');
        } else {
            res.send("USER IDnonono");

        }

    }, function (error) {
        console.log("ERRORE QUI");
        console.log(error);

    })
    console.log(response);

});

app.get('/sendtest', function (req, res) {

    var sendGrid = require( __dirname+ '/cloud/sendgrid-parse');
    sendGrid.initialize("hybridrecruitment", "k0st0golov");
    var email = sendGrid.Email({to:['lisophorm@gmail.com']});
    email.setFrom('null@void.com');
    email.setFromName('alfo');
    //email.addBcc(['lisophorm@gmail.com']);
    email.setHTML('<b>Please review and sign my timesheet: (document ID ' + 11 + ')</b> ');
    email.addFilter('templates', 'template_id', '3f4749a8-afd8-45f5-aabd-08069b304358');
    email.addUniqueArg('signDocID', '12');
    //email.header.sub=subs;
    email.addSubstitution("-companyName-", 'ciao');

    email.addSubstitution("-documentID-", 'gino34');
    email.addSubstitution("-currentpath-",'gino22');
    email.setSubject('Timesheet from alfo ');
    sendGrid.send(email).then(function (success) {
        res.send('email sent');
    }, function (fail) {
        console.log('eror');
        res.send(fail);
    })

})

app.get('/timesheet/:jobID', function (req, res) {
    console.log('timesheet');
    var timeSheetId = req.params.jobID.toString();
    console.log(timeSheetId);
    briefData = {};
    shortListData = {};
    Parse.Cloud.useMasterKey();
    var TimeSheet = Parse.Object.extend("SigninDocuments");
    var timeQuery = new Parse.Query(TimeSheet);
    timeQuery.equalTo('objectId', timeSheetId);
    timeQuery.first().then(function (response1) {
        if (typeof response1 !== 'undefined') {
            briefData = response1;
            res.render('timesheet', {
                message: 'fangu',
                weekModel: briefData.get('weekModel'),
                id: briefData.id,
                timeModel: briefData.get('timeModel'),
                totalTime: briefData.get('totalTime'),
                signerName: briefData.get('signerName'),
                signerEmail: briefData.get('signerEmail'),
                signerRole: briefData.get('signerRole'),
                projectTitle: briefData.get('projectTitle'),
                authorName: briefData.get('authorName'),
                authorEmail: briefData.get('authorEmail')


            });
        } else {
            res.send('Timesheet not found - Contact sysadmin ' + req.param('jobID'));
        }

    }, function (error) {
        res.send(error);
    });

});

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

app.get('/invoice/:jobID', function (req, res) {

    console.log('invoice zzz');
    var timeSheetId = req.params.jobID.toString();
    console.log(timeSheetId);
    briefData = {};
    shortListData = {};
    var TimeSheet = Parse.Object.extend("Invoices");
    var timeQuery = new Parse.Query(TimeSheet);
    timeQuery.equalTo('docID', timeSheetId);
    timeQuery.first().then(function (response1) {
        console.log('response');
        console.log(response1);
        if (typeof response1 !== 'undefined') {
            console.log('invoice found');

            res.render('invoice', {
                "billingAddress":nl2br(response1.get('billingAddress'),false),
                "items":response1.get('items'),
                "taxes":response1.get('taxes'),
                "totalCost":response1.get('totalCost'),
                "createdAt":response1.get('createdAt'),
                "invoiceNo":response1.get('invoiceNo'),
                "bankDetails":nl2br(response1.get('bankDetails')),
                "from": {
                    "title": "Fuse Inc.",
                    "address": "2810 Country Club Road Cranford, NJ 07016",
                    "phone": "+66 123 455 87",
                    "email": "hello@fuseinc.com",
                    "website": "www.fuseinc.com"
                },
                "client": {
                    "title": "John Doe",
                    "address": "9301 Wood Street Philadelphia, PA 19111",
                    "phone": "+55 552 455 87",
                    "email": "johndoe@mail.com"
                },
                "number": "P9-0004",
                "date": "Jul 19, 2015",
                "dueDate": "Aug 24, 2015",
                "services": [
                    {
                        "title": "Prototype & Design",
                        "detail": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan, quam sed eleifend imperdiet.",
                        "unit": "Hour",
                        "unitPrice": "12.00",
                        "quantity": "240",
                        "total": "2880"
                    },
                    {
                        "title": "Coding",
                        "detail": "Vestibulum ligula sem, rutrum et libero id, porta vehicula metus. Cras dapibus neque sit amet laoreet vestibulum.",
                        "unit": "Hour",
                        "unitPrice": "10.50",
                        "quantity": "350",
                        "total": "3675"
                    },
                    {
                        "title": "Testing",
                        "detail": "Pellentesque luctus efficitur neque in finibus. Integer ut nunc in augue maximus porttitor id id nulla. In vitae erat.",
                        "unit": "Hour",
                        "unitPrice": "4.00",
                        "quantity": "50",
                        "total": "200"
                    },
                    {
                        "title": "Documentation & Training",
                        "detail": "Pellentesque luctus efficitur neque in finibus. Integer ut nunc in augue maximus porttitor id id nulla. In vitae erat.",
                        "unit": "Hour",
                        "unitPrice": "6.50",
                        "quantity": "260",
                        "total": "1690"
                    }
                ],
                "subtotal": "8445",
                "tax": "675.60",
                "discount": "120.60",
                "total": "9000"
            });
        } else {
            console.log('invoice not found');
            res.send('invoice NOT found');
        }

    }, function (error) {
        res.send(error);
    });


});

app.get('/candidate/:userID', function (req, res) {
    console.log('candidate contacted');
    console.log(req.params.userID);
    briefData = {};
    shortListData = {};

    var utente = new Parse.Object("_User");
    utente.id = req.params.userID;


    res.render('candidate');

});

app.get('/joboffer/:jobID', function (req, res) {
    console.log('candidate contacted');
    console.log(req.params.jobID);
    console.log(req.params.candID);
    briefData = {};
    shortListData = {};


    var searchJob = new Parse.Query('JobBag');
    var searchPpl = new Parse.Query('JobPeople');
    searchJob.equalTo('objectId', req.param('jobID'));
    searchJob.first().then(function (response1) {
        if (typeof response1 !== 'undefined') {
            briefData = response1;
            res.render('jobposting', {
                message: 'fangu',
                internalCode: briefData.get('internalCode'),
                id: briefData.id,
                title: briefData.get('title'),
                description: briefData.get('jobDescription')


            });
        } else {
            res.send('Job Bag not found. Contact sysadmin');
        }

    }, function (error) {
        res.send(error);
    });

});

app.get('/joboffer/:jobID', function (req, res) {
    console.log('candidate contacted');
    console.log(req.params.jobID);
    console.log(req.params.candID);
    briefData = {};
    shortListData = {};


    var searchJob = new Parse.Query('JobBag');
    var searchPpl = new Parse.Query('JobPeople');
    searchJob.equalTo('objectId', req.param('jobID'));
    searchJob.first().then(function (response1) {
        if (typeof response1 !== 'undefined') {
            briefData = response1;
            res.render('jobposting', {
                message: 'fangu',
                internalCode: briefData.get('internalCode'),
                id: briefData.id,
                title: briefData.get('title'),
                description: briefData.get('jobDescription')


            });
        } else {
            res.send('Job Bag not found. Contact sysadmin');
        }

    }, function (error) {
        res.send(error);
    });

});

function spitFile(obj, res) {
    var fileInfo = {};
    console.log('found it');
    console.log(obj.get('attach').url());
    fileInfo.name = obj.get('attach').name();
    console.log(fileInfo.name);
    Parse.Cloud.httpRequest({
        method: 'GET',
        url: obj.get('attach').url()
    }).then(function (response) {
        res.setHeader('Content-disposition', 'attachment; filename=' + fileInfo.name);
        res.end(response.buffer.toString('binary'), 'binary');
        //res.send(response.buffer);
    }, function (error) {
        res.send(error);
    });

}

app.get('/getFile/:fileID', function (req, res) {

    var fileInfo = {};
    console.log(req.params.fileID);
    var EmailReports = Parse.Object.extend("WorkAttachments");
    var searchFile = new Parse.Query(EmailReports);
    searchFile.equalTo('objectId', req.params.fileID);
    searchFile.first({useMasterKey: true}).then(function (obj) {
        if (typeof obj !== 'undefined') {
            console.log('not undefined');
            spitFile(obj, res);
        } else {
            console.log('file not found');
            res.send('File not found. Contact sysadmin ');
        }
    }, function (error) {
        res.send(error);
    });

});

app.get('/approveCandidate/:fileID', function (req, res) {
    //826d976b-a945-45ee-8e35-a184b8da96f5
    var continueChain = true;
    var userObject = {};

    console.log("REQUEST USER ID:" + req.params.fileID);
    var queryCode = new Parse.Query('_User');
    queryCode.equalTo('objectId', req.params.fileID);
    queryCode.first({useMasterKey: true}).then(function (obj) {
        if (typeof obj !== 'undefined') {
            console.log("USER ID:");
            console.log(obj);
            userObject = obj;
            obj.set('registrationStatus', 'approved');

            return obj.save(null, {useMasterKey: true});
        } else {
            continueChain = false;
            console.log("USER IDnonono");
            res.send('no user found');
        }

    }, function (error) {
        if (continueChain) {
            continueChain = false;
            console.log("ERRORE QUI");
            console.log(error);
            res.send(error);
        }
    }).then(function (success) {
        if (continueChain) {
            console.log('sending email to');
            console.log(userObject.get('email'));
            var sendGrid = require("./cloud/sendgrid-parse");
            sendGrid.initialize("hybridrecruitment", "k0st0golov");

            var email = sendGrid.Email({to: [userObject.get('email')]});
            email.setFrom('admin@place-me.co.uk');
            email.setFromName('Place-me Admin');
            // email.addTo('lisophorm@gmail.com');
            email.setHTML('<b>Welcome on board and some super amazing welcome text.</b>');

            email.setSubject('Welcome to Place-ME!');

            return sendGrid.sendEmail(email);
        }
    }, function (error) {
        console.log('errir in calling user');
        console.log(error);
        if (continueChain) {
            continueChain = false;
            res.send(error);
        }
    }).then(function (success) {
        if (continueChain) {
            res.send('THis candidate is approved. In production you will be redirected to a landing page');
        }
    }, function (error) {
        if (continueChain) {
            res.send(error);
        }
    });

});

app.get('/approveClient/:fileID', function (req, res) {
    //826d976b-a945-45ee-8e35-a184b8da96f5
    var continueChain = true;
    var userObject = {};
    Parse.Cloud.useMasterKey();
    console.log("REQUEST USER ID:" + req.params.fileID);
    var queryCode = new Parse.Query('_User');
    queryCode.equalTo('objectId', req.params.fileID);
    queryCode.first({useMasterKey: true}).then(function (obj) {
        if (typeof obj !== 'undefined') {
            console.log("USER ID:");
            console.log(obj);
            userObject = obj;
            obj.set('registrationStatus', 'approved');

            return obj.save(null, {useMasterKey: true});
        } else {
            continueChain = false;
            console.log("USER IDnonono");
            res.send('no user found');
        }

    }, function (error) {
        if (continueChain) {
            continueChain = false;
            console.log("ERRORE QUI");
            console.log(error);
            res.send(error);
        }
    }).then(function (succos) {
        if (continueChain) {
            var query2 = new Parse.Query(Parse.Role);
            query2.equalTo("name", "Administrator");
            return query2.first({useMasterKey: true});
        }
    }, function (erroro) {
        console.log(erroro);
        if (continueChain) {
            continueChain = false;
            res.send(erroro);
        }
    }).then(function (role) {
            if (continueChain) {
                console.log('assigned admin');
                role.getUsers().add(userObject);
                return role.save(null, {useMasterKey: true});
            }
        }, function (erroro2) {
            console.log(erroro2);
            if (continueChain) {
                continueChain = false;
                res.send(erroro2);
            }
        })
        .then(function (success) {
            if (continueChain) {
                var sendGrid = require("./cloud/sendgrid-parse");
                sendGrid.initialize("hybridrecruitment", "k0st0golov");

                var email = sendGrid.Email({to: [userObject.get('email')]});
                email.setFrom('admin@place-me.co.uk');
                email.setFromName('Place-me Admin');
                email.addTo('lisophorm@gmail.com');
                email.setHTML('<b>Welcome on board and some super amazing welcome text.</b>');

                email.setSubject('Welcome to Place-ME! - CLIENT');

                return sendGrid.sendEmail(email);
            }
        }, function (error) {
            console.log('errir in calling user');
            console.log(error);
            if (continueChain) {
                continueChain = false;
                res.send(error);
            }
        }).then(function (success) {
        if (continueChain) {
            res.send('THis CLIENT is approved. Status of ADMIN is assigned to this user. In production you will be redirected to a landing page');
        }
    }, function (error) {
        if (continueChain) {
            res.send(error);
        }
    });

});


app.get('/createUserFromBackend/:email', function (req, res) {
    var newUserID = '';
    var continueChain = true;


    var queryCode = new Parse.Query('_User');
    queryCode.equalTo('username', req.params.email);
    queryCode.first({useMasterKey: true}).then(function (obj) {
        if (typeof obj !== 'undefined') {
            console.log("USER ID:");
            console.log(obj);
            userObject = obj;
            // obj.set('registrationStatus', 'approved');
            res.send('user already exists');
            return;
            // return obj.save(null,{ useMasterKey: true });
        } else {
            var User = Parse.Object.extend("_User");
            var user = new User();
            console.log("USER IDnonono");
            console.log( req.params.email);
            user.setUsername(req.params.email);
            user.setPassword( 'changeme');
            user.setEmail( req.params.email);
            user.set('firstname', 'changeme');
            user.set('lastname', 'changeme');
            user.set('newFromBackend', true);
            user.set('backend', true);
            return user.save(null, {useMasterKey: true});
        }

    }, function (error) {
        if (continueChain) {
            continueChain = false;
            console.log("ERRORE QUI");
            console.log(error);
            res.send(error);
        }
    }).then(function (succo) {
        console.log('NEW USER');
        console.log(succo.id);


        if (continueChain) {
            var UserInfo = Parse.Object.extend("UserInfo");

            var userInfo = new UserInfo();
            console.log('USERINFGO');
            userInfo.set('userID', succo);
            userInfo.set('email', req.params.email);
            userInfo.set('firstname', 'changeme');
            userInfo.set('lastname', 'changeme');
            return userInfo.save(null, {useMasterKey: true});
        } else {
            console.log('must stop');
        }
    }, function (erro) {
        if (continueChain) {
            console.log('erro iserinf');
            res.send(JSON.stringify(erro));
        } else {
            console.log('must stop');
        }
    }).then(function (succo2) {
        if (continueChain) {
            newUserID = succo2.id;
            res.redirect('/private-profile/' + newUserID);
        }
    }, function (erro2) {
        if (continueChain) {
            res.send(JSON.stringify(erro2));
        }
    });


    console.log(Parse.User.current());

    //Parse.Cloud.run('isAdmin',{currentUser:req.user.id}).then(function (ammo) {
    //res.send(ammo);
    // }, function (erro) {
    //   res.send(erro);
//  });
    //console.log('Fetched question: '+req.params.id);
    //

});

// digital signature

var signPassword = 'k0st0golov';
var signApikey = process.env.SIGNINHUB_KEY || '4F85C651E076CF804F4B04608E9B29129EBE64BFF795E17015D4B76CBB81B0C3';
var signcCient_id = 'placeme';
var signTemplatename = 'template02';
var signMasterUser = 'lisophorm@gmail.com';

app.get('/signTheContract/:id', function (req, res, next) {
    console.log(Parse.User.current());
    Parse.Cloud.run('isAdmin', {currentUser: req.user.id}).then(function (ammo) {
        res.send(ammo);
    }, function (erro) {
        res.send(erro);
    });
    //console.log('Fetched question: '+req.params.id);
    //res.redirect('http://www.google.com');

});

app.get('/signingHub', function (req, res) {
    var docu_status = '';
    var docObject = {};
    console.log(signcCient_id);
    var access_token = '';
    var SIgnTimeSheet = Parse.Object.extend("SigninDocuments");
    var continua = true;
    var queryCode = new Parse.Query(SIgnTimeSheet);
    queryCode.equalTo('signID', req.query.document_id);
    queryCode.first({useMasterKey: true}).then(function (result) {
        docObject = result;
        return Parse.Cloud.httpRequest({
            url: 'https://api.signinghub.com/authenticate',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            },
            body: 'grant_type=password&client_id=' + signcCient_id
            + '&client_secret=' + signApikey + '&username='
            + signMasterUser + '&password='
            + signPassword
        });

    }, function (error) {
        continua = false;
        res.send('error writing document info: ' + JSON.stringify(error));
    }).then(function (httpResponse) {
            access_token = httpResponse.data.access_token;
            if (continua) {
                return Parse.Cloud.httpRequest({
                    url: 'https://api.signinghub.com/v2/documents/' + req.query.document_id + '/log',
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + access_token

                    }
                });
            }

        },
        function (httpResponse) {
            if (continua) {
                continua = false;
                console.log('Failed with: ' + httpResponse.status);
                console.log(httpResponse);
                res.send(httpResponse.text);
            }
        }).then(function (cocco) {
        if (continua) {
            var actions = JSON.parse(cocco.text).actions;
            docu_status = JSON.parse(cocco.text).document_status;
            console.log('actions');
            console.log(actions);
            var no_res = true;
            for (var i = 0; i < actions.length; i++) {
                var action = actions[i];
                console.log("Action " + i + " " + action.action_type + " +" + action.information.value);
                if (action.action_type == "DECLINED_DOCUMENT") {
                    no_res = false;
                    docu_action = 'declined';
                    docObject.set('status', 'declined');
                    docObject.set('reason', action.information.value);
                    return docObject.save();
                    break;
                }
                if (action.action_type == "SIGNED") {
                    no_res = false;
                    docu_action = 'signed';
                    docObject.set('status', 'signed');
                    docObject.set('reason', docu_status);
                    return docObject.save();
                    break;
                }
            }
            console.log('actions length');
            console.log(actions.length);
            if (no_res) {
                no_res = false;

                docObject.set('status', action.action_type);
                docObject.set('reason', action.information.value);
                return docObject.save();

            }
        }

    }, function (erro) {
        if (continua) {
            continua = false;
            res.send(JSON.stringify(erro));
        }
    }).then(function (oggetto) {
        res.render("thankyou");
    }, function (error1) {
        if (continua) {
            res.send("Error while saving results - " + JSON.stringify(error1));
        }

    });

});


//** end of digital signature *//

//** callback for sendgrid **//


app.post('/sendGridHook', function (req, res) {

    console.log('sendgrid hook');
    console.log(req.body);
    if(typeof req.body ==='undefined') {
        res.send('POST EMPTY');

    }
    var reports = [];
    var EmailReports = Parse.Object.extend("EmailReports");
    for (var i = 0; i < req.body.length; i++) {
        var current = req.body[i];
        var emailReport = new EmailReports();
        emailReport.set(current);
        if (typeof current.candidateID !== 'undefined') {
            var searchJob = new Parse.Query('JobPeople');
            searchJob.equalTo('objectId', req.body[i].candidateID);
            searchJob.first().then(function (response1) {
                if (typeof response1 !== 'undefined') {
                    emailReport.set('nullo', '3');
                    response1.set('lastAction', current.event);
                    response1.set('lastActionTime', new Date());
                    response1.save();
                    emailReport.set('nullo', '3');
                } else {

                    emailReport.set('nullo', '5');
                }
            });
        }
        else {
            emailReport.set('nullo', JSON.stringify(current));
        }
        reports.push(emailReport);
    }
    Parse.Object.saveAll(reports , {
        useMasterKey: true}).
    then(function (success) {
        console.log('sendgrid event saved');
        // {useMasterKey: true}
        res.send('req saved');
    }, function (error) {
        console.log('sendgridHook error');
        console.log(error);
        res.send(error);
    });

//res.send(req.body);
    //res.send(JSON.stringify(req.code));

    //var queryCode = new Parse.Query('JobBag');
//  queryCode.first().then(function(obj) {
    //  res.send(JSON.stringify(obj));
    // });
});


//** end callback for sendgrid **//

/*********** LINKEDIN
 *
 *
 */
var restrictedAcl = new Parse.ACL();
restrictedAcl.setPublicReadAccess(false);
restrictedAcl.setPublicWriteAccess(false);

var linkedinClientId = '77c0sh4voemltg';
var linkedinClientSecret = 'TIKtuiRZ2xs6BKkw';

var linkedinRedirectEndpoint = 'https://www.linkedin.com/uas/oauth2/authorization?';
var linkedinValidateEndpoint = 'https://www.linkedin.com/uas/oauth2/accessToken';
var linkedinUserEndpoint = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,picture-url,email-address)';

var TokenRequest = Parse.Object.extend("TokenRequest");
var TokenStorage = Parse.Object.extend("TokenStorage");


var restrictedAcl = new Parse.ACL();
restrictedAcl.setPublicReadAccess(false);
restrictedAcl.setPublicWriteAccess(false);

app.get('/authorize', function (req, res) {
    console.log('within authorize function');
    var redirectUri = "https://app.place-me.co.uk/oauthCallback";
    var responseType = "code";
    var tokenRequest = new TokenRequest();
    // Secure the object against public access.
    tokenRequest.setACL(restrictedAcl);
    /**
     * Save this request in a Parse Object for validation when linkedin responds
     * Use the master key because this class is protected
     */
    tokenRequest.save(null, {useMasterKey: true}).then(function (obj) {
        /**
         * Redirect the browser to linkedin for authorization.
         * This uses the objectId of the new TokenRequest as the 'state'
         *   variable in the linkedin redirect.
         */
        res.redirect(
            linkedinRedirectEndpoint + querystring.stringify({
                client_id: linkedinClientId,
                state: obj.id,
                response_type: responseType,
                redirect_uri: redirectUri
            })
        );
    }, function (error) {
        // If there's an error storing the request, render the error page.
        res.render('error', {errorMessage: 'Failed to save auth request.'});
    });

});
app.get('/oauthCallback', function (req, res) {
    var data = req.query;
    var token;
    /**
     * Validate that code and state have been passed in as query parameters.
     * Render an error page if this is invalid.
     */
    if (!(data && data.code && data.state)) {
        res.render('error', {errorMessage: 'Invalid auth response received.'});
        return;
    }
    var query = new Parse.Query(TokenRequest);
    /**
     * Check if the provided state object exists as a TokenRequest
     * Use the master key as operations on TokenRequest are protected
     */
    Parse.Cloud.useMasterKey();
    Parse.Promise.as().then(function () {
        return query.get(data.state);
    }).then(function (obj) {
        // Destroy the TokenRequest before continuing.
        return obj.destroy();
    }).then(function () {
        // Validate & Exchange the code parameter for an access token from linkedin
        return getlinkedinAccessToken(data.code);
    }).then(function (access) {
        /**
         * Process the response from linkedin, return either the getlinkedinUserDetails
         *   promise, or reject the promise.
         */
        var linkedinData = access.data;
        if (linkedinData && linkedinData.access_token) {
            token = linkedinData.access_token;
            return getlinkedinUserDetails(token);
        } else {
            return Parse.Promise.error("Invalid access request.");
        }
    }).then(function (userDataResponse) {
        /**
         * Process the users linkedin details, return either the upsertlinkedinUser
         *   promise, or reject the promise.
         */
        /*var userDataXml = userDataResponse.text;
         $userDataXml = $( userDataXml ),
         var userData = $userDataXml.find('id');*/
        var userData = userDataResponse.data;
        console.log('linkedin user data:');
        console.log(userData);
        if (userData && userData.id) {
            return upsertlinkedinUser(token, userData);
        } else {
            return Parse.Promise.error("Unable to parse Linkedin data");
        }
    }).then(function (user) {
        /**
         * Render a page which sets the current user on the client-side and then
         *   redirects to /main
         */
        res.render('store_auth', {sessionToken: user.getSessionToken()});
    }, function (error) {
        /**
         * If the error is an object error (e.g. from a Parse function) convert it
         *   to a string for display to the user.
         */
        if (error && error.code && error.error) {
            error = error.code + ' ' + error.error;
        }
        res.render('error', {errorMessage: JSON.stringify(error)});
    });

});



//** end of core of functonalities **//






// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/test.html'));
});

// Parse Server plays nicely with the rest of your web routes
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/index.html'));
    //res.status(200).send('Make sure to star the parse-server repo on GitHub!');
});

var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function () {
    console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
